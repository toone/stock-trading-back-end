package pers.lmy.stockTrading.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import pers.lmy.stockTrading.entity.Order;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface OrderMapper {
    @Insert("insert into `order` values(0,#{userid},now(),#{stockcode}," +
            "#{stockname},#{stockprice},#{volume},#{totalprice},#{type})")
    void insert(@Param("userid") int userid,
            @Param("stockcode") String stockcode,
            @Param("stockname") String stockname,
            @Param("stockprice") BigDecimal stockprice,
            @Param("volume") int volume,
            @Param("totalprice") BigDecimal totalprice,
            @Param("type") int type);

    @Select("select * from `order` where userid = #{userid}")
    List<Order> selectByUserId(@Param("userid") int userid);
}