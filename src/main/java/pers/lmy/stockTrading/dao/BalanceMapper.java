package pers.lmy.stockTrading.dao;

import org.apache.ibatis.annotations.*;
import pers.lmy.stockTrading.entity.Balance;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface BalanceMapper {

    @Select("select * from balance where userid = #{userid}")
    List<Balance> selectByUserId(@Param("userid") int userid);

    @Update("update balance set balance = #{balance} where userid = #{userid}")
    void update(@Param("userid") int userid,
            @Param("balance") BigDecimal balance);

    @Insert("insert into balance values(#{userid},#{balance})")
    void insert(@Param("userid") int userid,
            @Param("balance") BigDecimal balance);
}