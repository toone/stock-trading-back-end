package pers.lmy.stockTrading.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import pers.lmy.stockTrading.entity.User;

import java.util.List;

@Mapper
public interface UserMapper {

    @Select("select * from user where username = #{username} and password = " +
            "#{password}")
    List<User> selectByUserNameAndPassword(@Param("username") String username,
            @Param("password") String password);

    @Select("select * from user where username = #{username} ")
    List<User> selectByUserName(@Param("username") String username);

    @Insert("insert into user values(0,#{username},#{password})")
    void insert(@Param("username") String username,
            @Param("password") String password);
}