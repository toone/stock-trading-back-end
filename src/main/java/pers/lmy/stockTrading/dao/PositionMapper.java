package pers.lmy.stockTrading.dao;

import org.apache.ibatis.annotations.*;
import pers.lmy.stockTrading.entity.Position;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface PositionMapper {
    @Select("select * from position where userid = #{userid}")
    List<Position> selectByUserId(@Param("userid") int userid);

    @Update("update position set number = #{number},cost = #{cost} where " +
            "userid = #{userid} " + "and stockcode = #{stockcode}")
    void update(@Param("userid") int userid,
            @Param("stockcode") String stockcode,
            @Param("cost") BigDecimal cost, @Param("number") int number);

    @Delete("delete from position where userid = #{userid} and stockcode = " +
            "#{stockcode}")
    void delete(@Param("userid") int userid,
            @Param("stockcode") String stockcode);

    @Insert("insert into position values(0,#{userid},#{stockcode},#" +
            "{stockname},#{cost},#{number})")
    void insert(@Param("userid") int userid,
            @Param("stockcode") String stockcode,
            @Param("stockname") String stockname,
            @Param("cost") BigDecimal cost, @Param("number") int number);
}