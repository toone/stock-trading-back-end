package pers.lmy.stockTrading.entity;

import java.math.BigDecimal;

public class Balance {
    private int  userid;

    private BigDecimal balance;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}