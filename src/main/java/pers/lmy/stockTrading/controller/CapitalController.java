package pers.lmy.stockTrading.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.lmy.stockTrading.entity.Balance;
import pers.lmy.stockTrading.entity.Position;
import pers.lmy.stockTrading.entity.User;
import pers.lmy.stockTrading.service.CapitalService;
import pers.lmy.stockTrading.service.StockService;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class CapitalController {
    @Autowired
    private CapitalService capitalService = new CapitalService();

    @PostMapping("/capital")
    public JSONObject getCapital(@RequestBody User user) {
        int userid = user.getUserid();
        return capitalService.getCapital(userid);
    }
}
