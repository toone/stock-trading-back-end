package pers.lmy.stockTrading.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pers.lmy.stockTrading.entity.Order;
import pers.lmy.stockTrading.entity.Position;
import pers.lmy.stockTrading.entity.User;
import pers.lmy.stockTrading.service.TradeService;

import java.util.List;

@RestController
@CrossOrigin
public class TradeController {
    @Autowired
    TradeService tradeService = new TradeService();

    @PostMapping("/buy")
    public String buy(@RequestBody Position position) {
        tradeService.buyStock(position.getUserid(), position.getStockcode(),
                              position.getStockname(), position.getCost(),
                              position.getNumber());
        return "success";
    }

    @PostMapping("/sell")
    public String sell(@RequestBody Position position) {
        tradeService.sellStock(position.getUserid(), position.getStockcode(),
                              position.getStockname(), position.getCost(),
                              position.getNumber());
        return "success";
    }

    @PostMapping("/orderList")
    public List<Order> getOrderList(@RequestBody User user) {
        return tradeService.getOrderList(user.getUserid());
    }
}
