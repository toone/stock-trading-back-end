package pers.lmy.stockTrading.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.lmy.stockTrading.service.StockService;

@RestController
@CrossOrigin
public class StockController {
    @Autowired
    private StockService service = new StockService();

    @PostMapping("/stockDaily")
    public JSONObject test(@RequestBody JSONObject jsonObject) {
        String stockCode = jsonObject.getString("stockCode");
        return service.getStockDaily(stockCode);
    }

    @PostMapping("/instantStockIndex")
    public JSONObject getInstantStockIndex(@RequestBody JSONObject jsonObject) {
        String stockCode = jsonObject.getString("stockCode");
        return service.getInstantStockIndex(stockCode);
    }

    @PostMapping("/instantStock")
    public JSONObject getInstantStock(@RequestBody JSONObject jsonObject) {
        String stockCode = jsonObject.getString("stockCode");
        return service.getInstantStock(stockCode);
    }
}
