package pers.lmy.stockTrading.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class StockService {
    public JSONObject getStockDaily(String ts_code) {
        //用fastJson实现json对象的操作
        JSONObject postData = new JSONObject();
        postData.put("api_name", "daily");
        postData.put("token",
                     "fd5cc4894988be42729b00feca903f4fd0e8979ac056da235d11027b");
        //多层嵌套json的实现方式：jsonObject套jsonObject
        JSONObject params = new JSONObject();
        params.put("ts_code", ts_code);
        postData.put("params", params);
        postData.put("fields", "trade_date,open,close,low,high,vol");

        RestTemplate client = new RestTemplate();
        JSONObject json =
                client.postForEntity("http://api.tushare.pro", postData,
                                     JSONObject.class).getBody();
        return json;
    }

    public JSONObject getInstantStockIndex(String stockCode) {
        //新浪实时数据接口需要添加header才能使用
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("referer", "http://finance.sina.com.cn");
        JSONObject postData = new JSONObject();
        HttpEntity httpEntity = new HttpEntity(postData, httpHeaders);
        RestTemplate client = new RestTemplate();
        String response = client.exchange(
                "http://hq.sinajs.cn/list=sh000001,sz399001,sz399006," +
                        stockCode, HttpMethod.GET, httpEntity, String.class)
                .getBody();
        //        String response = client.postForEntity(
        //                "http://hq.sinajs.cn/list=sh000001,sz399001,
        //                sz399006,"+stockCode,
        //                httpEntity, String.class).getBody();
        //对新浪财经接口传回的实时数据进行格式处理
        String[] data = response.split(";");
        JSONObject ans = new JSONObject();
        for (int i = 0; i < 3; ++i) {
            String[] temp = data[i].split("\"");
            String[] data0 = temp[1].split(",");
            JSONObject json = new JSONObject();
            json.put("证券简称", data0[0]);
            json.put("昨日收盘价", data0[2]);
            json.put("最近成交价", data0[3]);
            ans.put(data0[0], json);
        }
        String[] temp = data[3].split("\"");
        String[] data0 = temp[1].split(",");
        JSONObject json = new JSONObject();
        json.put("证券简称", data0[0]);
        json.put("昨日收盘价", data0[2]);
        json.put("最近成交价", data0[3]);
        ans.put("选中股票", json);
        return ans;
    }

    public JSONObject getInstantStock(String stockCode) {
        //新浪实时数据接口需要添加header才能使用
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("referer", "http://finance.sina.com.cn");
        JSONObject postData = new JSONObject();
        HttpEntity httpEntity = new HttpEntity(postData, httpHeaders);
        RestTemplate client = new RestTemplate();
        String response =
                client.exchange("http://hq.sinajs.cn/list=" + stockCode,
                                HttpMethod.GET, httpEntity, String.class)
                        .getBody();
        //对新浪财经接口传回的实时数据进行格式处理
        String[] data = response.split("\"");
        String[] data0 = data[1].split(",");
        JSONObject json = new JSONObject();
        json.put("证券简称", data0[0]);
        json.put("今开", new BigDecimal(data0[1]).stripTrailingZeros());
        json.put("昨收", new BigDecimal(data0[2]).stripTrailingZeros());
        json.put("现价", new BigDecimal(data0[3]).stripTrailingZeros());
        json.put("最高", new BigDecimal(data0[4]).stripTrailingZeros());
        json.put("最低", new BigDecimal(data0[5]).stripTrailingZeros());
        json.put("成交量", data0[8]);
        return json;
    }

    public Map<String, BigDecimal> getMultipledInstantStockPrice(
            List<String> stockList) {
        Map<String, BigDecimal> map = new HashMap<>();
        if (stockList.isEmpty()) {
            return map;
        }
        String str = "";
        for (String stock : stockList) {
            str += stock;
            str += ",";
        }
        //新浪实时数据接口需要添加header才能使用
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("referer", "http://finance.sina.com.cn");
        JSONObject postData = new JSONObject();
        HttpEntity httpEntity = new HttpEntity(postData, httpHeaders);
        RestTemplate client = new RestTemplate();
        String response = client.exchange("http://hq.sinajs.cn/list=" + str,
                                          HttpMethod.GET, httpEntity,
                                          String.class).getBody();
        String[] data = response.split(";");
        for (int i = 0; i < data.length - 1; ++i) {
            String[] temp = data[i].split("\"");
            String[] data0 = temp[1].split(",");
            BigDecimal price = new BigDecimal(data0[3]);
            price = price.stripTrailingZeros();
            map.put(stockList.get(i), price);
        }
        return map;
    }
}

