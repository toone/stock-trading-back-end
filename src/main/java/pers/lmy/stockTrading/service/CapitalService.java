package pers.lmy.stockTrading.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.lmy.stockTrading.dao.BalanceMapper;
import pers.lmy.stockTrading.dao.PositionMapper;
import pers.lmy.stockTrading.entity.Balance;
import pers.lmy.stockTrading.entity.Position;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class CapitalService {
    @Autowired
    StockService stockService = new StockService();
    @Autowired
    private BalanceMapper balanceMapper;
    @Autowired
    private PositionMapper positionMapper;

    public JSONObject getCapital(int userid) {
        List<Balance> balanceList = balanceMapper.selectByUserId(userid);
        List<Position> positionList = positionMapper.selectByUserId(userid);
        BigDecimal balance = balanceList.get(0).getBalance();
        List<String> stockList = new ArrayList<>();
        for (Position position : positionList) {
            stockList.add(position.getStockcode());
        }
        Map<String, BigDecimal> map =
                stockService.getMultipledInstantStockPrice(stockList);
        NumberFormat percent = NumberFormat.getPercentInstance();
        percent.setMaximumFractionDigits(2);

        BigDecimal stockTotalCost = new BigDecimal(0);
        BigDecimal stockTotalPrice = new BigDecimal(0);
        JSONArray positionData = new JSONArray();
        //将持仓股票数据逐条放入json
        for (Position position : positionList) {
            int number = position.getNumber();
            BigDecimal instantPrice = map.get(position.getStockcode());
            BigDecimal totalPrice =
                    instantPrice.multiply(new BigDecimal(number));
            BigDecimal totalCost =
                    position.getCost().multiply(new BigDecimal(number));
            stockTotalCost = stockTotalCost.add(totalCost);
            stockTotalPrice = stockTotalPrice.add(totalPrice);

            JSONObject json = new JSONObject();
            json.put("stockCode", position.getStockcode());
            json.put("stockName", position.getStockname());
            json.put("instantPrice", instantPrice);
            json.put("cost", position.getCost());
            json.put("number", position.getNumber());
            json.put("instantTotalPrice", totalPrice);
            json.put("profit", totalPrice.subtract(totalCost));
            positionData.add(json);
        }
        //将资产数据格式化
        JSONObject capitalData = new JSONObject();
        capitalData.put("总资产", stockTotalPrice.add(balance));
        capitalData.put("总市值", stockTotalPrice);
        BigDecimal profit = stockTotalPrice.subtract(stockTotalCost);
        capitalData.put("账户盈亏", profit);
        BigDecimal ratio = new BigDecimal(0);
        if (stockTotalCost.compareTo(BigDecimal.ZERO) == 0) {
        } else {
            ratio = profit.divide(stockTotalCost, 4, BigDecimal.ROUND_HALF_UP);
        }
        capitalData.put("盈亏比率", percent.format(ratio.doubleValue()));
        BigDecimal positionRatio = stockTotalCost
                .divide(stockTotalCost.add(balance), 4,
                        BigDecimal.ROUND_HALF_UP);
        capitalData.put("当前仓位", percent.format(positionRatio.doubleValue()));
        capitalData.put("可用资金", balance);

        JSONObject ans = new JSONObject();
        ans.put("positionData", positionData);
        ans.put("capitalData", capitalData);
        return ans;
    }
}
