package pers.lmy.stockTrading.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.lmy.stockTrading.dao.BalanceMapper;
import pers.lmy.stockTrading.dao.UserMapper;
import pers.lmy.stockTrading.entity.User;

import java.math.BigDecimal;
import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private BalanceMapper balanceMapper;

    public List<User> login(String username, String password) {
        return userMapper.selectByUserNameAndPassword(username, password);
    }

    public List<User> register(String username, String password) {
        List<User> users = userMapper.selectByUserName(username);
        if (users.isEmpty()) {
            userMapper.insert(username,password);
            List<User> temp=userMapper.selectByUserName(username);
            int userid=temp.get(0).getUserid();
            balanceMapper.insert(userid,new BigDecimal(500000));
        }
        return users;
    }
}
